# Affiche 1

exercice num 1 de CGM

affiche A3 pour l'impression.
sujet: 

carte openstreetmap indiquant les lieux de squats de Bxl.

Todo:

-[X] get initial content

-[X] get bxl squats in marker.json

-[X] get html to render on top of the canvas

-[] center in print on Bxl

-[] try OpenLayers instead of Leaflet

