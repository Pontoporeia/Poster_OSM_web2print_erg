var markers = [
  {
    name: 'Brussel Xtreme Park',
    lat: 50.883618,
    lng: 4.418277,
  },
  {
    name: 'La Clef',
    lat: 50.880432,
    lng: 4.343833,
  },
  {
    name: 'Le Boiler',
    lat: 50.877172,
    lng: 4.340060,
  },
  {
    name: 'Les pompes',
    lat: 50.865116,
    lng: 4.371962,
  },
  {
    name: 'Poissonnerie',
    lat: 50.861726,
    lng: 4.361152,
  },
  {
    name: 'USPR',
    lat: 50.863211,
    lng: 4.349106,
  },
  {
    name: "L'Adesif",
    lat: 50.851370,
    lng: 4.375081,
  },
  {
    name: 'Flambeau',
    lat: 50.844523,
    lng: 4.405892,
  },
  {
    name: 'LaMAB',
    lat: 50.851892,
    lng: 4.365836,
  },
  {
    name: "L'Éventail",
    lat: 50.847693,
    lng: 4.379674,
  },
  {
    name: "L'Éventail",
    lat: 50.847693,
    lng: 4.379674,
  },
  {
    name: "Le Buurtwinkel",
    lat: 50.844341,
    lng: 4.343781,
  },
  {
    name: "La Bougie",
    lat: 50.847259,
    lng: 4.334559,
  },
  {
    name: "AbC",
    lat: 50.844427,
    lng: 4.333934,
  },
  {
    name: "La Finca",
    lat: 50.816417,
    lng: 4.435802,
  },
  {
    name: "Warmbed",
    lat: 50.825016,
    lng: 4.385215,
  },
  {
    name: "Naast Monique",
    lat: 50.838815,
    lng: 4.319548,
  },
  {
    name: "Café Solidaire",
    lat: 50.811941,
    lng: 4.391242,
  },
  {
    name: "Atoma",
    lat: 50.823079,
    lng: 4.329754,
  },
  {
    name: "L'Accroche",
    lat: 50.823761,
    lng: 4.323944,
  },
  {
    name: "Belgium Kitchen",
    lat: 50.813744,
    lng: 4.324525,
  },
  {
    name: "AS-OS",
    lat: 50.800012,
    lng: 4.336326,
  },
  {
    name: "Zonneklopper",
    lat: 50.805578,
    lng: 4.314698,
  },
  {
    name: "La Cheminée",
    lat: 50.802669,
    lng: 4.317836,
  },

]